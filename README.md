# StarWarsBib

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.7.

## Intro
This is a Star Wars Library.
All data is provided by [swapi.dev](https://swapi.dev/)

In this responsive Application you can:

 - navigate through Films, Planets and Characters of Star Wars
 - see details of all Films, Planets and Characters
 - open a fake form to create new Planets
 - search through all Films, Planets and Characters

Unfortunately the api delivers no pretty Star Wars images.
To reduce the amount of api requests, it is possible to implement a caching service.

To run it local:
 - download rep
 - run `npm install` in project folder
 - run `npx ng serve`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
