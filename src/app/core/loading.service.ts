import { Injectable } from '@angular/core';

import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';

@Injectable( {
  providedIn: 'root',
} )
export class LoadingService {
  public isLoading$: Observable<boolean>;
  private readonly DELAY: number = 200;

  private readonly isLoadingSubject: BehaviorSubject<boolean>        = new BehaviorSubject<boolean>( false );
  private readonly isLoadingDelayedSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>( false );

  public constructor() {
    this.isLoading$ =
      combineLatest(
        [
          this.isLoadingSubject.asObservable(),
          this.isLoadingDelayedSubject.asObservable().pipe( delay( this.DELAY ) ),
        ],
      )
        .pipe( map( ( values: boolean[] ) => values.every( ( value: boolean ) => value ) ) );
  }

  public start(): void {
    this.isLoadingSubject.next( true );
    this.isLoadingDelayedSubject.next( true );
  }

  public done(): void {
    this.isLoadingSubject.next( false );
    this.isLoadingDelayedSubject.next( false );
  }
}
