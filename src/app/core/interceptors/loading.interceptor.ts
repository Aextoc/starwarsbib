import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';

import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { LoadingService } from '../loading.service';

@Injectable()
export class LoadingInterceptor implements HttpInterceptor {
  public constructor(
    private readonly loadingService: LoadingService,
  ) {
  }

  public intercept( req: HttpRequest<any>, next: HttpHandler ): Observable<HttpEvent<any>> { /*tslint:disable-line:no-any*/
    if ( req.url.indexOf( '/api/' ) === -1 ) {
      return next.handle( req );
    }

    this.loadingService.start();

    return next.handle( req ).pipe( finalize( () => this.loadingService.done() ) );
  }
}
