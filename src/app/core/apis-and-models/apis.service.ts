import { Inject, Injectable, InjectionToken, Optional } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { forkJoin, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { FilmModel } from '@core/apis-and-models/film-model';
import { PeopleModel } from '@core/apis-and-models/people-model';
import { PlanetModel } from '@core/apis-and-models/planet-model';

export const API_BASE_URL: InjectionToken<string> = new InjectionToken<string>( 'API_BASE_URL' );

@Injectable( {
  providedIn: 'root',
} )
export class ApisService {

  private http: HttpClient;
  private readonly baseUrl: string;

  constructor( @Inject( HttpClient ) http: HttpClient, @Optional() @Inject( API_BASE_URL ) baseUrl?: string ) {
    this.http = http;
    this.baseUrl = baseUrl ? baseUrl : '';
  }

  public getFilms(): Observable<FilmModel[]> {
    const url: string = this.baseUrl + 'films/';
    return this.http.get( url )
      .pipe(
        map( ( res: any ) => {
          return res.results as FilmModel[];
        } ),
      );
  }

  public getFilmDetail( id: string ): Observable<FilmModel> {
    const url: string = `${this.baseUrl}films/${id}/`;
    return this.http.get( url )
      .pipe(
        map( ( res: any ) => {
          const film: FilmModel = res as FilmModel;
          film.id = parseInt( id, 10 );

          return film;
        } ),
      );
  }

  public getPeople(): Observable<PeopleModel[]> {
    const url: string = this.baseUrl + 'people/';
    return this.http.get( url )
      .pipe(
        map( ( res: any ) => {
          return res.results as PeopleModel[];
        } ),
      );
  }

  public getPeopleDetail( id: string ): Observable<PeopleModel> {
    const url: string = `${this.baseUrl}people/${id}/`;
    return this.http.get( url )
      .pipe(
        map( ( res: any ) => {
          const people: PeopleModel = res;
          people.id = parseInt( id, 10 );
          return res as PeopleModel;
        } ),
      );
  }

  public getPlanets(): Observable<PlanetModel[]> {
    const url: string = this.baseUrl + 'planets/';
    return this.http.get( url )
      .pipe(
        map( ( res: any ) => {
          return res.results as PlanetModel[];
        } ),
      );
  }

  public getPlanetDetail( id: string ): Observable<PlanetModel> {
    const url: string = `${this.baseUrl}planets/${id}/`;
    return this.http.get( url )
      .pipe(
        map( ( res: any ) => {
          const planet: PlanetModel = res;
          planet.id = parseInt( id, 10 );
          return planet;
        } ),
      );
  }

  public getMultipleData( urls: string[] ): Observable<Object[]> {
    return forkJoin(
      urls.map( ( url: string ) => {
        return this.http.get( url.replace( 'http://', 'https://' ) );
      } ),
    );
  }

  public addIdFromUrl( data: any ): any {
    return data.map( ( item: any ) => {
      const id: number | null = this.getIdFromUrl( item.url );
      if ( id !== null ) {
        item.id = id;
      }

      return item;
    } );
  }

  public getIdFromUrl( url: string ): number | null {
    const regex: any = /\d+/gm;
    const id: any = url.match( regex );
    return id ? parseInt( id[0], 10 ) : null;
  }
}
