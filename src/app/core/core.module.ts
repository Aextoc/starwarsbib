import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { MenuComponent } from '@core/components/menu/menu.component';
import { LoadingInterceptor } from '@core/interceptors/loading.interceptor';
import { SharedModule } from '@shared/shared.module';

@NgModule( {
  declarations: [
    MenuComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    ReactiveFormsModule,
  ],
  exports: [
    MenuComponent,
  ],
  providers:    [
    {
      provide:  HTTP_INTERCEPTORS,
      useClass: LoadingInterceptor,
      multi:    true,
    },
  ],
} )
export class CoreModule { }
