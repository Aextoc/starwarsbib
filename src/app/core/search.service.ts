import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { BehaviorSubject, Observable } from 'rxjs';

import { API_BASE_URL, ApisService } from '@core/apis-and-models/apis.service';

@Injectable( {
  providedIn: 'root',
} )
export class SearchService {


  public readonly searchResults$: Observable<any[]>;

  private searchResultsList: any[] = [];
  private readonly searchResults: BehaviorSubject<any[]> = new BehaviorSubject<any[]>( [] );
  private readonly baseUrl: string;

  public constructor(
    private readonly router: Router,
    private readonly apiService: ApisService,
    @Inject( API_BASE_URL ) baseUrl?: string,
  ) {
    this.baseUrl = baseUrl ? baseUrl : '';
    this.searchResults$ = this.searchResults.asObservable();
  }

  public startSearch( searchTerm: string ): void {
    this.searchResultsList = [];
    const searchTermList: string[] = [
      `${this.baseUrl}people/?search=${searchTerm}`,
      `${this.baseUrl}planets/?search=${searchTerm}`,
      `${this.baseUrl}films/?search=${searchTerm}`,
    ];

    const searchResults$: Observable<Object[]> = this.apiService.getMultipleData( searchTermList );

    searchResults$.subscribe(
      ( res: any[] ) => {
        res.forEach( ( item: any, index: number ) => {
          const category: string = index === 0 ? 'peoples' : index === 1 ? 'planets' : 'films';
          item.results.forEach( ( obj: any ) => {
            if ( obj !== undefined ) {
              obj.category = category;
              obj.id = this.apiService.getIdFromUrl( obj.url );
              this.searchResultsList.push( obj );
            }
          } );
        } );
        this.setSearchResults( this.searchResultsList );
        this.navigateToSearch();
      },
      ( error: Error ) => {
        console.log( error );
        this.searchResultsList.push( { error: 'error' } );
        this.setSearchResults( this.searchResultsList );
        this.navigateToSearch();
      },
    );
  }

  public navigateToSearch(): void {
    this.router.navigate( [ 'search' ], {
    } ).catch( () => { /* error while routing */ } );
  }

  public setSearchResults( values: any[] ): void {
    this.searchResults.next( values );
  }
}
