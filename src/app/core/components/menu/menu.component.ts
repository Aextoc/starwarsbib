import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Routes } from '@angular/router';


import { routes } from '@feature/app-routing.module';
import { SearchService } from '@core/search.service';

@Component( {
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
} )
export class MenuComponent  {

  public searchForm: FormGroup = new FormGroup(
    {
      searchText: new FormControl( null ),
    },
  );

  public routes: Routes = routes.filter( ( item: any ) => {
    if ( item.data !== undefined && item.path !== undefined ) {
      return item;
    }
  } );

  public constructor(
    private readonly searchService: SearchService,
  ) {}


  public onSearch(): void {
    if ( !( this.searchForm.value.searchText === '' || this.searchForm.value.searchText === null ) ) {
      this.searchService.startSearch( this.searchForm.value.searchText );
    }
  }
}
