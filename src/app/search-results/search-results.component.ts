import { Component, OnDestroy } from '@angular/core';
import { SearchService } from '@core/search.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component( {
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.scss'],
} )
export class SearchResultsComponent implements OnDestroy {

  public searchResults: any[] | undefined;
  public isError: boolean = false;

  private readonly isDestroyed: Subject<boolean> = new Subject<boolean>();

  constructor(
    private readonly searchService: SearchService,
  ) {
    this.searchService.searchResults$
      .pipe( takeUntil( this.isDestroyed ) )
      .subscribe(
        ( res: any[] ) => {
          if ( res.find( ( item: any ) => item.error === 'error' ) || res.length < 1 ) {
            this.isError = true;
          }
          else {
            this.isError = false;
            this.searchResults = res;
          }
      },
        ( error: Error ) => {
          this.isError = true;
          console.log( error );
        } );
  }

  public ngOnDestroy(): void {
    this.isDestroyed.next( true );
    this.isDestroyed.complete();
  }
}
