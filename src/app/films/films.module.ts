import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@shared/shared.module';
import { FilmsRoutingModule } from './films-routing.module';
import { FilmListComponent } from './film-list/film-list.component';
import { FilmDetailComponent } from './film-detail/film-detail.component';


@NgModule ( {
  declarations: [
    FilmListComponent,
    FilmDetailComponent,
  ],
  imports: [
    CommonModule,
    FilmsRoutingModule,
    SharedModule,
  ],
} )
export class FilmsModule { }
