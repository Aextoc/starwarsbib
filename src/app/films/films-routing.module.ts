import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FilmListComponent } from '@feature/films/film-list/film-list.component';
import { FilmDetailComponent } from '@feature/films/film-detail/film-detail.component';


const routes: Routes = [
  {
    path: '',
    component: FilmListComponent,
    pathMatch: 'full',
  },
  {
    path: 'details',
    component: FilmDetailComponent,
  },
  {
    path: '**',
    component: FilmListComponent,
  },
];

@NgModule( {
  imports: [RouterModule.forChild( routes )],
  exports: [RouterModule],
} )
export class FilmsRoutingModule { }
