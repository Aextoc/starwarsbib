import { Component, OnInit } from '@angular/core';

import { ApisService } from '@core/apis-and-models/apis.service';
import { FilmModel } from '@core/apis-and-models/film-model';

@Component( {
  selector: 'app-film-list',
  templateUrl: './film-list.component.html',
  styleUrls: ['./film-list.component.scss'],
} )
export class FilmListComponent implements OnInit {

  public films: FilmModel[] | undefined;
  public isError: boolean = false;

  constructor(
      private apiService: ApisService,
    ) {
  }

  ngOnInit(): void {
    this.getFilms();
  }

  private getFilms(): void {
    this.apiService.getFilms().subscribe(
      ( films: FilmModel[] ) => {
        this.films = this.apiService.addIdFromUrl( films );
        this.films = films.sort( ( a: FilmModel, b: FilmModel ) => {
          return a.episode_id - b.episode_id;
        } );
      },
      ( error: Error ) => {
        this.isError = true;
        console.log( error );
      },
    );
  }
}
