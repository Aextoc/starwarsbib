import { ChangeDetectorRef, Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FilmModel } from '@core/apis-and-models/film-model';
import { ApisService } from '@core/apis-and-models/apis.service';
import { PeopleModel } from '@core/apis-and-models/people-model';
import { PlanetModel } from '@core/apis-and-models/planet-model';

@Component( {
  selector: 'app-people-detail',
  templateUrl: './film-detail.component.html',
  styleUrls: ['./film-detail.component.scss'],
} )
export class FilmDetailComponent implements OnDestroy {

  public filmDetails: FilmModel | undefined;
  public isNoDetails: boolean = false;
  public isLoading: boolean = false;

  public characters: any[] = [];
  public planets: any[] = [];
  public starships: any[] = [];
  public vehicles: any[] = [];

  private readonly isDestroyed: Subject<boolean> = new Subject<boolean>();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private apiService: ApisService,
    private readonly changeDetectorRef: ChangeDetectorRef,
  ) {
    if ( this.router.getCurrentNavigation() !== null ) {
      const routeData: any = this.router.getCurrentNavigation();
      if ( routeData !== null ) {
        this.filmDetails = routeData.extras.state;
      }
      if ( routeData.extras.state === undefined ) {
        this.isLoading = true;

        const id: string | null = this.route.snapshot.paramMap.get( 'id' );
        if ( id !== null ) {
          this.apiService.getFilmDetail( id )
            .pipe( takeUntil( this.isDestroyed ) )
            .subscribe(
            ( res: any ) => {
              this.filmDetails = res;
              this.getFilmDetails( this.filmDetails );
              this.isLoading = false;
              this.isNoDetails = false;
              this.changeDetectorRef.detectChanges();
            },
            ( error: Error ) => {
              this.isLoading = false;
              this.isNoDetails = true;
              console.log( error );
            },
          );
        }
      }
    }

    if ( this.filmDetails !== undefined ) {
      this.getFilmDetails( this.filmDetails );
    }
    else {
      // maybe add a cache service
      this.isLoading = true;
    }
  }

  public ngOnDestroy(): void {
    this.isDestroyed.next( true );
    this.isDestroyed.complete();
  }

  private getFilmDetails( filmDetails: any ): any {
    const characters$: Observable<Object[]> = this.apiService.getMultipleData( filmDetails.characters );
    const starships$: Observable<Object[]>  = this.apiService.getMultipleData( filmDetails.starships );
    const vehicles$: Observable<Object[]>   = this.apiService.getMultipleData( filmDetails.vehicles );
    const planets$: Observable<Object[]>    = this.apiService.getMultipleData( filmDetails.planets );

    characters$.pipe( takeUntil( this.isDestroyed ) ).subscribe(
      ( res: any ) => {
        const characters: PeopleModel[] = res as PeopleModel[];
        this.characters = this.apiService.addIdFromUrl( characters );
      },
      ( error: Error ) => {
        console.log( error );
      },
    );

    starships$.pipe( takeUntil( this.isDestroyed ) ).subscribe(
      ( res: any ) => {
        this.starships = res;
      },
      ( error: Error ) => {
        console.log( error );
      },
    );

    vehicles$.pipe( takeUntil( this.isDestroyed ) ).subscribe(
      ( res: any ) => {
        this.vehicles = res;
      },
      ( error: Error ) => {
        console.log( error );
      },
    );

    planets$.pipe( takeUntil( this.isDestroyed ) ).subscribe(
      ( res: any ) => {
        const planets: PlanetModel[] = res as PlanetModel[];
        this.planets = this.apiService.addIdFromUrl( planets );
      },
      ( error: Error ) => {
        console.log( error );
      },
    );
  }
}
