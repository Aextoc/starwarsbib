import { NgModule } from '@angular/core';

import { ButtonModule } from 'primeng/button';
import { CardModule } from 'primeng/card';
import { ProgressBarModule } from 'primeng/progressbar';
import { InputTextModule } from 'primeng/inputtext';
import { MultiSelectModule } from 'primeng/multiselect';

import { DialogModule } from 'primeng/dialog';
import { DetailsPageComponent } from './details-page/details-page.component';
import { RomanNumberConverterPipe } from './pipes/roman-number-converter.pipe';

const DECLARATIONS: ReadonlyArray<any> = [
  ButtonModule,
  CardModule,
  ProgressBarModule,
  DialogModule,
  InputTextModule,
  MultiSelectModule,
];

@NgModule( {
  imports: [],
  declarations: [
    DetailsPageComponent,
    RomanNumberConverterPipe,
  ],
  exports: [
    ...DECLARATIONS,
    DetailsPageComponent,
    RomanNumberConverterPipe,
  ],
} )
export class SharedModule { }
