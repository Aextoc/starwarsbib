import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PeopleDetailComponent } from '@feature/peoples/people-detail/people-detail.component';
import { PeopleListComponent } from '@feature/peoples/people-list/people-list.component';


const routes: Routes = [
  {
    path: '',
    component: PeopleListComponent,
    pathMatch: 'full',
  },
  {
    path: 'details',
    component: PeopleDetailComponent,
  },
  {
    path: '**',
    component: PeopleListComponent,
  },
];

@NgModule( {
  imports: [RouterModule.forChild( routes )],
  exports: [RouterModule],
} )
export class PeoplesRoutingModule { }
