import { ChangeDetectorRef, Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { ApisService } from '@core/apis-and-models/apis.service';
import { PeopleModel } from '@core/apis-and-models/people-model';
import { PlanetModel } from '@core/apis-and-models/planet-model';
import { FilmModel } from '@core/apis-and-models/film-model';

@Component( {
  selector: 'app-people-detail',
  templateUrl: './people-detail.component.html',
  styleUrls: ['./people-detail.component.scss'],
} )
export class PeopleDetailComponent implements OnDestroy {

  public peopleDetails: PeopleModel | undefined;
  public isNoDetails: boolean = false;
  public isLoading: boolean = false;

  public homeworld: any[] = [];
  public films: any[] = [];
  public starships: any[] = [];
  public species: any[] = [];
  public vehicles: any[] = [];

  private readonly isDestroyed: Subject<boolean> = new Subject<boolean>();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private apiService: ApisService,
    private readonly changeDetectorRef: ChangeDetectorRef,
  ) {
    if ( this.router.getCurrentNavigation() !== null ) {
      const routeData: any = this.router.getCurrentNavigation();
      if ( routeData !== null ) {
        this.peopleDetails = routeData.extras.state;
      }
      if ( routeData.extras.state === undefined ) {
        this.isLoading = true;

        const id: string | null = this.route.snapshot.paramMap.get( 'id' );
        if ( id !== null ) {
          this.apiService.getPeopleDetail( id )
            .pipe( takeUntil( this.isDestroyed ) )
            .subscribe(
            ( res: any ) => {
              this.peopleDetails = res;
              this.getPeopleDetails( this.peopleDetails );
              this.isLoading = false;
              this.isNoDetails = false;
              this.changeDetectorRef.detectChanges();
            },
            ( error: Error ) => {
              this.isLoading = false;
              this.isNoDetails = true;
              console.log( error );
            },
          );
        }
      }
    }

    if ( this.peopleDetails !== undefined ) {
      this.getPeopleDetails( this.peopleDetails );
    }
    else {
      // maybe add a cache service
      this.isLoading = true;
    }
  }

  public ngOnDestroy(): void {
    this.isDestroyed.next( true );
    this.isDestroyed.complete();
  }

  private getPeopleDetails( peopleDetails: any ): any {
    const homeworld$: Observable<Object[]> = this.apiService.getMultipleData( [ peopleDetails.homeworld ] );
    const films$: Observable<Object[]>     = this.apiService.getMultipleData( peopleDetails.films );
    const species$: Observable<Object[]>   = this.apiService.getMultipleData( peopleDetails.species );
    const vehicles$: Observable<Object[]>  = this.apiService.getMultipleData( peopleDetails.vehicles );
    const starships$: Observable<Object[]> = this.apiService.getMultipleData( peopleDetails.starships );

    homeworld$.pipe( takeUntil( this.isDestroyed ) ).subscribe(
      ( res: any ) => {
        const homeworld: PlanetModel[] = res as PlanetModel[];
        this.homeworld = this.apiService.addIdFromUrl( homeworld );
      },
      ( error: Error ) => {
        console.log( error );
      },
    );
    films$.pipe( takeUntil( this.isDestroyed ) ).subscribe(
      ( res: any ) => {
        const films: FilmModel[] = res as FilmModel[];
        this.films = this.apiService.addIdFromUrl( films );
      },
      ( error: Error ) => {
        console.log( error );
      },
    );

    species$.pipe( takeUntil( this.isDestroyed ) ).subscribe(
      ( res: any ) => {
        this.species = res;
      },
      ( error: Error ) => {
        console.log( error );
      },
    );

    vehicles$.pipe( takeUntil( this.isDestroyed ) ).subscribe(
      ( res: any ) => {
        this.vehicles = res;
      },
      ( error: Error ) => {
        console.log( error );
      },
    );

    starships$.pipe( takeUntil( this.isDestroyed ) ).subscribe(
      ( res: any ) => {
        this.starships = res;
      },
      ( error: Error ) => {
        console.log( error );
      },
    );
  }
}
