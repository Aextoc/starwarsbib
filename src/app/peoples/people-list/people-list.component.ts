import { Component, OnDestroy, OnInit } from '@angular/core';

import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { ApisService } from '@core/apis-and-models/apis.service';
import { PeopleModel } from '@core/apis-and-models/people-model';

@Component( {
  selector: 'app-people-list',
  templateUrl: './people-list.component.html',
  styleUrls: ['./people-list.component.scss'],
} )
export class PeopleListComponent implements OnInit, OnDestroy {

  public peoples: PeopleModel[] | undefined;
  public isError: boolean = false;

  private readonly isDestroyed: Subject<boolean> = new Subject<boolean>();

  constructor(
      private apiService: ApisService,
    ) {
  }

  public ngOnInit(): void {
    this.getPeople();
  }

  public ngOnDestroy(): void {
    this.isDestroyed.next( true );
    this.isDestroyed.complete();
  }

  private getPeople(): void {
    this.apiService.getPeople()
      .pipe( takeUntil( this.isDestroyed ) )
      .subscribe(
      ( peoples: PeopleModel[] ) => {
        this.peoples = this.apiService.addIdFromUrl( peoples );
      },
      ( error: Error ) => {
        this.isError = true;
        console.log( error );
      },
    );
  }
}
