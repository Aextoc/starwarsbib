import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@shared/shared.module';
import { PeoplesRoutingModule } from './peoples-routing.module';
import { PeopleDetailComponent } from '@feature/peoples/people-detail/people-detail.component';
import { PeopleListComponent } from '@feature/peoples/people-list/people-list.component';


@NgModule( {
  declarations: [
    PeopleDetailComponent,
    PeopleListComponent,
  ],
  imports: [
    CommonModule,
    PeoplesRoutingModule,
    SharedModule,
  ],
} )
export class PeoplesModule { }
