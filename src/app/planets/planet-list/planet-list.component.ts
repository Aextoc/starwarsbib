import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';

import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { ApisService } from '@core/apis-and-models/apis.service';
import { PlanetModel } from '@core/apis-and-models/planet-model';

@Component( {
  selector: 'app-people-list',
  templateUrl: './planet-list.component.html',
  styleUrls: ['./planet-list.component.scss'],
} )
export class PlanetListComponent implements OnInit, OnDestroy {

  // dummy data
  public filmsOptions: any = [
    {name: 'The Phantom Menace'},
    {name: 'Attack of the Clones' },
    {name: 'Revenge of the Sith'},
    {name: 'A New Hope' },
    {name: 'The Empire Strikes Back'},
    {name: 'Return of the Jedi'},
  ];
  selectedFilms: any;

  public planets: PlanetModel[] | undefined;
  public isError: boolean = false;
  public displayDialog: boolean = false;
  public isPlanetCreated: boolean = false;

  private readonly isDestroyed: Subject<boolean> = new Subject<boolean>();

  constructor(
      private apiService: ApisService,
      private readonly changeDetectorRef: ChangeDetectorRef,
    ) {
  }

  public ngOnInit(): void {
    this.getPlanets();
  }

  public ngOnDestroy(): void {
    this.isDestroyed.next( true );
    this.isDestroyed.complete();
  }

  public addNewPlanet(): void {
    this.displayDialog = true;
    this.isPlanetCreated = false;
  }

  public closeDialog(): void {
    this.displayDialog = false;
    this.changeDetectorRef.detectChanges();
  }

  public createPlanet(): void {
    // Todo: get form data and send data, when create api ready
    this.isPlanetCreated = true;
    setTimeout(
      () => {
        this.displayDialog = false;
        this.changeDetectorRef.detectChanges();
      },
      1500,
    );
  }

  private getPlanets(): void {
    this.apiService.getPlanets()
      .pipe( takeUntil( this.isDestroyed ) )
      .subscribe(
      ( planets: PlanetModel[] ) => {
        this.planets = this.apiService.addIdFromUrl( planets );
      },
      ( error: Error ) => {
        this.isError = true;
        console.log( error );
      },
    );
  }
}
