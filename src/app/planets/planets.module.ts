import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlanetsRoutingModule } from './planets-routing.module';
import { SharedModule } from '@shared/shared.module';
import { PlanetDetailComponent } from '@feature/planets/planet-detail/planet-detail.component';
import { PlanetListComponent } from '@feature/planets/planet-list/planet-list.component';
import { FormsModule } from '@angular/forms';


@NgModule( {
  declarations: [
    PlanetDetailComponent,
    PlanetListComponent,
  ],
  imports: [
    CommonModule,
    PlanetsRoutingModule,
    SharedModule,
    FormsModule,
  ],
} )
export class PlanetsModule { }
