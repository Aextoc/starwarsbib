import { ChangeDetectorRef, Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { ApisService } from '@core/apis-and-models/apis.service';
import { PeopleModel } from '@core/apis-and-models/people-model';
import { PlanetModel } from '@core/apis-and-models/planet-model';
import { FilmModel } from '@core/apis-and-models/film-model';

@Component( {
  selector: 'app-planet-detail',
  templateUrl: './planet-detail.component.html',
  styleUrls: ['./planet-detail.component.scss'],
} )
export class PlanetDetailComponent implements OnDestroy {

  public planetDetails: PlanetModel | undefined;
  public isNoDetails: boolean = false;
  public isLoading: boolean = false;

  public films: any[] = [];
  public residents: any[] = [];

  private readonly isDestroyed: Subject<boolean> = new Subject<boolean>();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private apiService: ApisService,
    private readonly changeDetectorRef: ChangeDetectorRef,
  ) {
    if ( this.router.getCurrentNavigation() !== null ) {
      const routeData: any = this.router.getCurrentNavigation();
      if ( routeData !== null ) {
        this.planetDetails = routeData.extras.state;
      }
      if ( routeData.extras.state === undefined ) {
        this.isLoading = true;

        const id: string | null = this.route.snapshot.paramMap.get( 'id' );
        if ( id !== null ) {
          this.apiService.getPlanetDetail( id )
            .pipe( takeUntil( this.isDestroyed ) )
            .subscribe(
            ( res: any ) => {
              this.planetDetails = res;
              this.getPlanetDetails( this.planetDetails );
              this.isLoading = false;
              this.isNoDetails = false;
              this.changeDetectorRef.detectChanges();
            },
            ( error: Error ) => {
              this.isLoading = false;
              this.isNoDetails = true;
              console.log( error );
            },
          );
        }
      }
    }

    if ( this.planetDetails !== undefined ) {
      this.getPlanetDetails( this.planetDetails );
    }
    else {
      this.isLoading = true;
    }
  }

  public ngOnDestroy(): void {
    this.isDestroyed.next( true );
    this.isDestroyed.complete();
  }

  private getPlanetDetails( planetDetails: any ): any {
    const films$: Observable<Object[]>     = this.apiService.getMultipleData( planetDetails.films );
    const residents$: Observable<Object[]> = this.apiService.getMultipleData( planetDetails.residents );

    films$.pipe( takeUntil( this.isDestroyed ) ).subscribe(
      ( res: any ) => {
        const films: FilmModel[] = res as FilmModel[];
        this.films = this.apiService.addIdFromUrl( films );
      },
      ( error: Error ) => {
        console.log( error );
      },
    );

    residents$.pipe( takeUntil( this.isDestroyed ) ).subscribe(
      ( res: any ) => {
        const residents: PeopleModel[] = res as PeopleModel[];
        this.residents = this.apiService.addIdFromUrl( residents );
      },
      ( error: Error ) => {
        console.log( error );
      },
    );
  }
}
