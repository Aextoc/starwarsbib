import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlanetDetailComponent } from '@feature/planets/planet-detail/planet-detail.component';
import { PlanetListComponent } from '@feature/planets/planet-list/planet-list.component';


const routes: Routes = [
  {
    path: '',
    component: PlanetListComponent,
    pathMatch: 'full',
  },
  {
    path: 'details',
    component: PlanetDetailComponent,
  },
  {
    path: '**',
    component: PlanetListComponent,
  },
];

@NgModule( {
  imports: [RouterModule.forChild( routes )],
  exports: [RouterModule],
} )
export class PlanetsRoutingModule { }
