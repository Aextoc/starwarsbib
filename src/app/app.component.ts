import { ChangeDetectorRef, Component } from '@angular/core';

import { LoadingService } from '@core/loading.service';

@Component( {
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
} )
export class AppComponent {

  public isLoading: boolean = false;

  constructor(
    private readonly loadingService: LoadingService,
    private readonly changeDetectorRef: ChangeDetectorRef,
  ) {
    this.loadingService.isLoading$.subscribe( ( isLoading: boolean ) => {
      this.isLoading = isLoading;
      // if some consecutive webapi calls are slow, the delay in the loading service will interfere with change detection cycles
      // to play it safe detectChanges is used
      this.changeDetectorRef.detectChanges();
    } );
  }
}
