import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '@feature/home/home.component';
import { SearchResultsComponent } from '@feature/search-results/search-results.component';


export const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    pathMatch: 'full',
  },
  {
    path: 'search',
    component: SearchResultsComponent,
  },
  {
    path: 'films',
    // tslint:disable-next-line:typedef
    loadChildren: () => import( './films/films.module' ).then( m => m.FilmsModule ),
    data: { title: 'Filme'},
  },
  {
    path: 'planets',
    // tslint:disable-next-line:typedef
    loadChildren: () => import( './planets/planets.module' ).then( m => m.PlanetsModule ),
    data: { title: 'Planeten'},
  },
  {
    path: 'peoples',
    // tslint:disable-next-line:typedef
    loadChildren: () => import( './peoples/peoples.module' ).then( m => m.PeoplesModule ),
    data: { title: 'Charaktere'},
  },
  {
    path: '**',
    component: HomeComponent,
  },
];

@NgModule( {
  imports: [RouterModule.forRoot(
    routes,
    {
      initialNavigation: true,
      enableTracing: false,
      onSameUrlNavigation: 'reload',
    },
  ), ],
  exports: [RouterModule],
} )
export class AppRoutingModule { }
